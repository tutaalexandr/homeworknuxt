import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

const store = () =>
  new Vuex.Store({
    state: {
      user: null
    },
    // mutations: {
    //   increment(state) {
    //     state.counter++
    //   }
    // }
  })

export default store
